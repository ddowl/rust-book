use std::collections::HashMap;
use std::str;
use std::io;

fn main() {
    print_get_stats(&[1, 2, 3]);
    print_get_stats(&[1, 2, 3, 4, 5, 6, 7, 8, 9]);
    print_get_stats(&[9, 8, 7, 6, 5, 4, 3, 2, 1]);
    print_get_stats(&[9, 9, 9, 9, 9, 9, 9, 9, 9]);
    println!();

    print_pig_latin("");
    print_pig_latin("pig latin");
    print_pig_latin("this is an undecipherable secret message");
    println!();

//    add_employee("Add Drew to Engineering");
//    add_employee("Add Yvonne to Engineering");
//    add_employee("Add Matt to Medical");
//    println!("{:?}", get_employees_from_department("Engineering"));
//    println!("{:?}", get_employees_from_department("Medical"));
//    println!("{:?}", get_employees_from_department("Sales"));

    company_interface();
}

/*
    Given a list of integers, use a vector and return the mean (the average value), median (when
    sorted, the value in the middle position), and mode (the value that occurs most often; a hash
    map will be helpful here) of the list.
*/
fn get_stats(ints: &[i32]) -> (f32, i32, i32) {
    let mut mean: f32 = 0.0;
    for num in ints.iter() {
        mean += *num as f32;
    }
    mean /= ints.len() as f32;
    

    let copy = ints.to_owned();
    let median = copy[copy.len() / 2];

    let mut counts = HashMap::new();
    for num in ints.iter() {
        let count = counts.entry(num).or_insert(0);
        *count += 1;
    }
    let mut mode = 0;
    let mut max_count = 0;
    for (num, count) in &counts {
        if *count > max_count {
            max_count = *count;
            mode = **num;
        }
    }

    (mean, median, mode)
}

fn print_get_stats(ints: &[i32]) {
    println!("get_stats({:?}) = {:?}", ints, get_stats(ints));
}

/*
    Convert strings to pig latin. The first consonant of each word is moved to the end of the word
    and “ay” is added, so “first” becomes “irst-fay.” Words that start with a vowel have “hay” added
    to the end instead (“apple” becomes “apple-hay”). Keep in mind the details about UTF-8 encoding!
*/
fn pig_latin(s: &str) -> String {
    let mut res = String::from("");
//    let words = s.split(' ').collect();
    for word in s.split_whitespace() {  // iterate over
        let pig_word = pig_latin_word(word);

        if !res.is_empty() {
            res.push(' ');
        }
        res.push_str(&pig_word[..]);
    }
    res
}

fn pig_latin_word(word: &str) -> String {
    let vowels = ['a', 'e', 'i', 'o', 'u'];
    let mut chars = word.chars();

    // has the side effect of 'consuming' first char
    let first_letter: char = chars.next().unwrap();

    if vowels.contains(&first_letter) {
        format!("{}-hay", word)
    } else {
        format!("{}-{}ay", chars.as_str(), first_letter)  // chars doesn't contain first letter
    }
}

fn print_pig_latin(s: &str) {
    println!("pig_latin({:?}) = {:?}", s, pig_latin(s));
}

/*
    Using a hash map and vectors, create a text interface to allow a user to add employee names to a
    department in a company. For example, “Add Sally to Engineering” or “Add Amir to Sales.” Then
    let the user retrieve a list of all people in a department or all people in the company by
    department, sorted alphabetically.
*/
fn company_interface() {

    let mut directory: HashMap<String, Vec<String>> = HashMap::new();

    println!("This is the greatest and best Company in the world...");
    loop {
        println!("Please input your action: 'add' or 'view'");

        let mut action = String::new();
        io::stdin().read_line(&mut action)
            .expect("Failed to read line");

        match action.trim() {
            "add" => {
                add_command(&mut directory);
            },
            "view" => {
                get_command(&directory);
            },
            _ => continue
        }
    }
}

fn add_command(directory: &mut HashMap<String, Vec<String>>) -> Result<(), ()> {
    let mut command_input = String::new();
    println!("Input add command...");
    println!("'Add <Person X> to <Department Y>'");
    io::stdin().read_line(&mut command_input).unwrap();
    add_employee_to_directory(directory, command_input)?;
    println!("Inserted successfully!");
    Ok(())
}

fn get_command(directory: &HashMap<String, Vec<String>>) -> Result<(), ()> {
    let mut command_input = String::new();
    println!("Which department do you want to view?");
    io::stdin().read_line(&mut command_input).unwrap();
    let employees_in_department = get_employees_from_department(directory, command_input)?;
    println!("{:?}", employees_in_department);
    Ok(())
}


fn add_employee_to_directory(directory: &mut HashMap<String, Vec<String>>, request_str: String) -> Result<(), ()> {
    let words: Vec<&str> = request_str.split_whitespace().collect();
    if words.len() != 4 || words[0] != "Add" || words[2] != "to" {
        return Err(());
    }

    let employee = words[1].to_string();
    let department = words[3].to_string();

    let employees_in_department = directory.entry(department).or_insert(Vec::new());
    employees_in_department.push(employee);
    Ok(())
}

fn get_employees_from_department(directory: &HashMap<String, Vec<String>>, department: String) -> Result<&Vec<String>,()> {
    match directory.get(department.trim()) {
        Some(employees) => Ok(employees),
        None => Err(())
    }
}
