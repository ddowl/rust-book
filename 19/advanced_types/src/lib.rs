// Using the Newtype Pattern for Type Safety and Abstraction
// newtypes are a lightweight way to encapsulate implementation details

struct Newtype(i32);

impl Newtype {
    fn do_a_thing(self) {
        println!("{}", self.0);
    }
}

fn this_func_only_takes_newtypes(val: Newtype) {
    val.do_a_thing();
}


// Creating Type Synonyms with Type Aliases

fn type_syn_example() {
    // the "type" keyword is similar to "typedef" in C
    // the types are aliased to be the same thing, and can
    // be used interchangeably

    type Kilometers = i32;

    let x: i32 = 5;
    let y: Kilometers = 5;

    println!("x + y = {}", x + y);
}



#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn newtype_funcs_dont_take_interior_type() {
        this_func_only_takes_newtypes(Newtype(5));
//        this_func_only_takes_newtypes(5);
    }


}
