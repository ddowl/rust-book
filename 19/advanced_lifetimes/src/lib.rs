// Lifetime Subtyping
struct Context<'s>(&'s str);

struct Parser<'c, 's: 'c> {
    context: &'c Context<'s>,
}

impl<'c, 's> Parser<'c, 's> {
    fn parse(&self) -> Result<(), &'s str> {
        Err(&self.context.0[1..])
    }
}

fn parse_context(context: Context) -> Result<(), &str> {
    Parser { context: &context }.parse()
}


// Lifetime Bounds on References to Generics
struct Ref<'a, T: 'a>(&'a T);

struct StaticRef<T: 'static>(&'static T);
